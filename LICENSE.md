Copyright (c) 2017 by eplab.xyz All rights reserved.

<p>Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:</p>

<ul>
<li>Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.</li>

<li>Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.</li>

<li>Neither the name of the NanoHttpd organization nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.</li>
</ul>

<p>THIS LICENSE IS IMPERMANENT AND THIS CAN BE CHANGED IN FUTURE.</p>