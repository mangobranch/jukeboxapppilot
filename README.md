# Jukebox App #
The Android App in Java from [eplab.io](http://eplab.io).

Jukebox is a small music platform that you can choose and listen to music.

Listen anything what you want with jukebox using extra smartphone.

## How To Use ##
### 1) Scanning Music List ###
![Screenshot_2017-03-25-10-02-34.png](https://bitbucket.org/repo/zEzMxM/images/2373607449-Screenshot_2017-03-25-10-02-34.png) ![Screenshot_2017-03-25-10-02-57.png](https://bitbucket.org/repo/zEzMxM/images/2723585641-Screenshot_2017-03-25-10-02-57.png) ![Screenshot_2017-03-25-10-03-06.png](https://bitbucket.org/repo/zEzMxM/images/3512275878-Screenshot_2017-03-25-10-03-06.png) ![Screenshot_2017-03-25-10-03-32.png](https://bitbucket.org/repo/zEzMxM/images/744949623-Screenshot_2017-03-25-10-03-32.png) ![Screenshot_2017-03-25-10-04-25.png](https://bitbucket.org/repo/zEzMxM/images/865955567-Screenshot_2017-03-25-10-04-25.png)

### 2) Configuring & Starting The Server ###
![Screenshot_2017-03-25-10-03-44.png](https://bitbucket.org/repo/zEzMxM/images/2689550889-Screenshot_2017-03-25-10-03-44.png) ![Screenshot_2017-03-25-10-04-09.png](https://bitbucket.org/repo/zEzMxM/images/1008928943-Screenshot_2017-03-25-10-04-09.png)

### 3) Printing QA Code ###
1. open qr-code web page with your server address. 
   for example, type 'http://{your-jukebox-server-address}/qr.html' in your browser address window

![QR Code.png](https://bitbucket.org/repo/zEzMxM/images/3325113890-QR%20Code.png)

2. Print QR Code for scanning & requesting your music list with your smart phone
   (QR Code has Requesting Page URL)
![Print QR Code.png](https://bitbucket.org/repo/zEzMxM/images/2881819457-Print%20QR%20Code.png)

### 4) Requesting Listening to Jukebox Music via QR Code ###
![IMG_2110.PNG](https://bitbucket.org/repo/zEzMxM/images/3439246815-IMG_2110.PNG) ![IMG_2111.PNG](https://bitbucket.org/repo/zEzMxM/images/2442167977-IMG_2111.PNG) ![IMG_2112.PNG](https://bitbucket.org/repo/zEzMxM/images/359182910-IMG_2112.PNG)
### Development Environment ###
* jdk 7
* Android 4.4([KITKAT](https://developer.android.com/reference/android/os/Build.VERSION_CODES.html?hl=ko#KITKAT))


### Used 3rd-party libraries ###
* [NanoHTTPD](http://nanohttpd.org)
* [Jaudiotagger](http://www.jthink.net/jaudiotagger/)


# License #
Custom License By [eplab.io](http://eplab.io)