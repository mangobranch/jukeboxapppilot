package branch.mango.jukeboxpilot.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by joe on 16. 11. 26.
 */

public class DBManager extends SQLiteOpenHelper {

    public DBManager(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE code( code TEXT, value TEXT);");
        db.execSQL("CREATE TABLE shop( id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, tblCnt INTEGER, wifiNm TEXT, wifiPwd TEXT, serverFlag TEXT);");
        db.execSQL("CREATE TABLE music( id INTEGER, path TEXT, name TEXT, size INTEGER, artistName TEXT, albumName TEXT, PRIMARY KEY('id'));");
        db.execSQL("CREATE TABLE playlog( time TEXT, m_path TEXT, PRIMARY KEY('time'));");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public JSONArray select(String query) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        JSONArray resultSet = new JSONArray();
        cursor.moveToFirst();
        while (cursor.isAfterLast() == false) {
            int totalColumn = cursor.getColumnCount();
            JSONObject rowObject = new JSONObject();
            for (int i = 0; i < totalColumn; i++) {
                if (cursor.getColumnName(i) != null) {
                    try {
                        rowObject.put(cursor.getColumnName(i),
                                cursor.getString(i));
                    } catch (Exception e) {
                        Log.d("DBManager > ", e.getMessage());
                    }
                }
            }
            resultSet.put(rowObject);
            cursor.moveToNext();
        }

        cursor.close();
        db.close();

        return resultSet;
    }
    public JSONObject selectOne(String query) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        JSONObject result = new JSONObject();

        cursor.moveToFirst();
        int totalColumn = cursor.getColumnCount();
        for (int i = 0; i < totalColumn; i++) {
            if (cursor.getColumnName(i) != null) {
                try {
                    result.put(cursor.getColumnName(i), cursor.getString(i));
                } catch (Exception e) {
                    Log.d("DBManager > ", e.getMessage());
                }
            }
        }

        cursor.close();
        db.close();

        return result;
    }

    public void insert(String query) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL(query);
        db.close();
    }

    public void update(String query) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL(query);
        db.close();
    }

    public void delete(String query) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL(query);
        db.close();
    }

    public SQLiteDatabase getDatabase() {
        return getWritableDatabase();
    }
}
