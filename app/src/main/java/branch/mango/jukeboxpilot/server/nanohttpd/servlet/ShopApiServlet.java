package branch.mango.jukeboxpilot.server.nanohttpd.servlet;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONObject;

import branch.mango.jukeboxpilot.Constants;
import branch.mango.jukeboxpilot.db.DBManager;

/**
 * Created by joe on 16. 11. 29.
 */

public class ShopApiServlet {

    private DBManager dbManager;

    public ShopApiServlet(Context context) {
        this.dbManager = new DBManager(context, Constants.SQLITE_DB, null, Constants.SQLITE_VERSION);
    }

    public JSONArray service() {

        JSONArray shopList = this.dbManager.select("select * from shop;");

        return shopList;
    }
}
