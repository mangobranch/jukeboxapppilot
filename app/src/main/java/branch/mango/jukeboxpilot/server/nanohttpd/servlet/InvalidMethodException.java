package branch.mango.jukeboxpilot.server.nanohttpd.servlet;

/**
 * Created by joe on 16. 12. 4.
 */

public class InvalidMethodException extends Exception {
    public InvalidMethodException() {
    }

    public InvalidMethodException(String message) {
        super(message);
    }

    public InvalidMethodException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidMethodException(Throwable cause) {
        super(cause);
    }
}
