package branch.mango.jukeboxpilot.server.network;

import android.os.AsyncTask;
import android.util.Log;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;

import static android.content.ContentValues.TAG;

public class Ping  extends AsyncTask<Void, Void,  Void> {

        private InetAddress in=null;
        private String ip=null;

        public void setIp(String ip){
            this.ip=ip;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try
            {
                try {

                    in = InetAddress.getByName(ip);
                    System.out.println("getHostName:"+in.getHostName());
                    try {
                        if (in.isReachable(1000)) {
                            System.out.println("Response ok:"+in.getHostName());
                        } else {
                            System.out.println("no Response:"+in.getHostName());
                        }
                        scanSubNet("192.168.10.");
                    } catch (Exception e) {
                        // TODO Auto-generated catch block

                    }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return null;
        }

        private ArrayList<String> scanSubNet(String subnet){
            ArrayList<String> hosts = new ArrayList<String>();

            InetAddress inetAddress = null;
            for(int i=100; i<120; i++){
                Log.d(TAG, "Trying: " + subnet + String.valueOf(i));
                try {
                    inetAddress = InetAddress.getByName(subnet + String.valueOf(i));
                    if(inetAddress.isReachable(1000)){
                        hosts.add(inetAddress.getHostName());
                        Log.d(TAG, inetAddress.getHostName());
                    }
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return hosts;
        }

    }
