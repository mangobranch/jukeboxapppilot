package branch.mango.jukeboxpilot.server.network;


import android.content.Context;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;


public class StaticIp {
    private     WifiConfiguration   wifiConf    = null;
    private     WifiManager         wifiManager = null;

    public void setWifiConf(WifiConfiguration wifiConf) {
        this.wifiConf = wifiConf;
    }

    StaticIp(WifiManager wm){
        this.wifiManager = wm;
    }


    public  void getWifiConf() {
        WifiInfo connectionInfo = wifiManager.getConnectionInfo();
        List<WifiConfiguration> configuredNetworks = wifiManager.getConfiguredNetworks();
        for (WifiConfiguration conf : configuredNetworks) {
            if (conf.networkId == connectionInfo.getNetworkId()) {
                wifiConf = conf;
                break;
            }
        }
    }

    public  void restartWifi(){
        wifiManager.setWifiEnabled(false);
        wifiManager.setWifiEnabled(true);
    }

    public  void setWifi(String setIp){

        getWifiConf();

        try{
            setIpAssignment("STATIC", wifiConf); //or "DHCP" for dynamic setting
            setIpAddress(InetAddress.getByName(setIp), 24, wifiConf);
            // setGateway(InetAddress.getByName("192.168.10.1"), wifiConf);
            // setDNS(InetAddress.getByName("210.220.163.82"), wifiConf);
            wifiManager.updateNetwork(wifiConf);
            wifiManager.saveConfiguration(); //Save
        }catch(Exception e){
            e.printStackTrace();
        }

        restartWifi();
    }


    public  void setIpAssignment(String assign , WifiConfiguration wifiConf)
            throws SecurityException, IllegalArgumentException, NoSuchFieldException, IllegalAccessException{
        setEnumField(wifiConf, assign, "ipAssignment");
    }

    public  void setIpAddress(InetAddress addr, int prefixLength, WifiConfiguration wifiConf)
            throws SecurityException, IllegalArgumentException, NoSuchFieldException, IllegalAccessException,
            NoSuchMethodException, ClassNotFoundException, InstantiationException, InvocationTargetException {
        Object linkProperties = getField(wifiConf, "linkProperties");
        if(linkProperties == null)return;
        Class laClass = Class.forName("android.net.LinkAddress");
        Constructor laConstructor = laClass.getConstructor(new Class[]{InetAddress.class, int.class});
        Object linkAddress = laConstructor.newInstance(addr, prefixLength);

        ArrayList mLinkAddresses = (ArrayList)getDeclaredField(linkProperties, "mLinkAddresses");
        mLinkAddresses.clear();
        mLinkAddresses.add(linkAddress);
    }

    public  void setGateway(InetAddress gateway, WifiConfiguration wifiConf)
            throws SecurityException, IllegalArgumentException, NoSuchFieldException, IllegalAccessException,
            ClassNotFoundException, NoSuchMethodException, InstantiationException, InvocationTargetException{
        Object linkProperties = getField(wifiConf, "linkProperties");
        if(linkProperties == null)return;
        Class routeInfoClass = Class.forName("android.net.RouteInfo");
        Constructor routeInfoConstructor = routeInfoClass.getConstructor(new Class[]{InetAddress.class});
        Object routeInfo = routeInfoConstructor.newInstance(gateway);

        ArrayList mRoutes = (ArrayList)getDeclaredField(linkProperties, "mRoutes");
        mRoutes.clear();
        mRoutes.add(routeInfo);
    }

    public  void setDNS(InetAddress dns, WifiConfiguration wifiConf)
            throws SecurityException, IllegalArgumentException, NoSuchFieldException, IllegalAccessException{
        Object linkProperties = getField(wifiConf, "linkProperties");
        if(linkProperties == null)return;

        ArrayList<InetAddress> mDnses = (ArrayList<InetAddress>)getDeclaredField(linkProperties, "mDnses");
        mDnses.clear(); //or add a new dns address , here I just want to replace DNS1
        mDnses.add(dns);
    }

    public  Object getField(Object obj, String name)
            throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException{
        Field f = obj.getClass().getField(name);
        Object out = f.get(obj);
        return out;
    }

    public  Object getDeclaredField(Object obj, String name)
            throws SecurityException, NoSuchFieldException,
            IllegalArgumentException, IllegalAccessException {
        Field f = obj.getClass().getDeclaredField(name);
        f.setAccessible(true);
        Object out = f.get(obj);
        return out;
    }

    public  void setEnumField(Object obj, String value, String name)
            throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException{
        Field f = obj.getClass().getField(name);
        f.set(obj, Enum.valueOf((Class<Enum>) f.getType(), value));
    }

    /*public  void setIpold(){

        android.provider.Settings.System.putString(getContentResolver(),android.provider.Settings.System.WIFI__IP,"192.168.10.104");
        android.provider.Settings.System.putString(getContentResolver(),android.provider.Settings.System.WIFI__DNS1, "192.168.1.1");
        android.provider.Settings.System.putString(getContentResolver(),android.provider.Settings.System.WIFI__GATEWAY, "192.168.10.1");
        android.provider.Settings.System.putString(getContentResolver(),android.provider.Settings.System.WIFI__NETMASK, "255.255.255.0");
        android.provider.Settings.System.putString(getContentResolver(),android.provider.Settings.System.WIFI_USE__IP, "1");
    }*/

}

