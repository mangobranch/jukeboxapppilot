package branch.mango.jukeboxpilot.server.nanohttpd.servlet;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import branch.mango.jukeboxpilot.Constants;
import branch.mango.jukeboxpilot.music.Music;
import branch.mango.jukeboxpilot.music.MusicManager;
import branch.mango.jukeboxpilot.db.DBManager;
import fi.iki.elonen.NanoHTTPD;

/**
 * Created by joe on 16. 12. 4.
 */

public class MusicApiServlet {
    private DBManager dbManager;

    public MusicApiServlet(Context context) {
        this.dbManager = new DBManager(context, Constants.SQLITE_DB, null, Constants.SQLITE_VERSION);
    }

    public JSONArray serviceMusicList(Map<String, String> parameters) throws Exception {
        ArrayList<Music> poolList = MusicManager.getInstatance().getMusicPoolList();

        JSONArray returnList = new JSONArray();

        String g = null; // find by musician name
        String q = null; // find by song name
        if (parameters.containsKey("q")) {
            q = parameters.get("q");
        }
        /*if (parameters.containsKey("g") && parameters.containsKey("q")) {
            g = parameters.get("g");
            q = parameters.get("q");
        }*/

        for (Music music : poolList) {
            JSONObject data = new JSONObject();

            data.put("id", music.getId());
            data.put("path", music.getPath());
            data.put("name", music.getTitle());

            if (q == null) {
                returnList.put(data);
            }
            else {
                if (music.getTitle().contains(q)) {
                    returnList.put(data);
                }
            }
        }

        return returnList;
    }
    public JSONArray serviceQueueing() throws Exception {
        List<Music> queueList = MusicManager.getInstatance().getMusicQueueList();

        JSONArray returnList = new JSONArray();
        for (Music music : queueList) {
            JSONObject data = new JSONObject();
            data.put("id", music.getId());
            data.put("path", music.getPath());
            data.put("name", music.getTitle());

            returnList.put(data);
        }

        return returnList;
    }

    public boolean alreadyGotMusicInQueueList(Map<String, String> parameters) {
        boolean checker = false;

        if (parameters.containsKey("q")) {
            String idStr = parameters.get("q");
            List<Music> queueList = MusicManager.getInstatance().getMusicQueueList();

            for (Music music : queueList) {
                if (Integer.valueOf(idStr).intValue() == music.getId()) {
                    checker = true;
                    break;
                }
            }
        }

        return checker;
    }

    private Object lock = new Object();

    public JSONArray addIntoQueue(NanoHTTPD.Method method, Map<String, String> parameters) throws Exception {

        if (NanoHTTPD.Method.POST.equals(method)) {

            String selectedParam = parameters.get("q");
            JSONArray selected = new JSONArray(selectedParam);
            ArrayList<Music> poolList = MusicManager.getInstatance().getMusicPoolList();

            synchronized(lock) {
                for (int i = 0; i < selected.length(); i++) {
                    for (Music music : poolList) {
                        if (music.getId() == (int) selected.get(i)) {
                            MusicManager.getInstatance().setMusicQueueList(music);
                            break;
                        }
                    }
                }
            }
        }
        else {
            throw new InvalidMethodException("Adding Music into Queue must be POST request");
        }

        return this.serviceQueueing();
    }
}
