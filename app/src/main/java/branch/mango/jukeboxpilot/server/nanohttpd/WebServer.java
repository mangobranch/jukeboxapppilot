package branch.mango.jukeboxpilot.server.nanohttpd;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import branch.mango.jukeboxpilot.Constants;
import branch.mango.jukeboxpilot.server.nanohttpd.servlet.MusicApiServlet;
import branch.mango.jukeboxpilot.server.nanohttpd.servlet.ShopApiServlet;
import fi.iki.elonen.NanoHTTPD;

/**
 * Created by joe on 16. 11. 26.
 */

public class WebServer extends NanoHTTPD {
    final private String LOGTAG = "[Mango:WebServer] ";

    private Context context;
    private AssetManager assetManager;

    public WebServer(int port) {
        super(port);
    }
    public WebServer(String hostname, int port) {
        super(hostname, port);
    }

    public final Map<String, String> getParms(Map<String, List<String>> parms) {
        Map<String, String> result = new HashMap<String, String>();
        for (String key : parms.keySet()) {
            result.put(key, parms.get(key).get(0));
        }

        return result;
    }

    public void setContext(Context context) {
        this.context = context;
    }
    public void setAssesManager(AssetManager assesManager) { this.assetManager = assesManager; }

    /**
     * Override this to customize the server.
     * <p/>
     * <p/>
     * (By default, this returns a 404 "Not Found" plain text error response.)
     *
     * @param session
     *            The HTTP session
     * @return HTTP response, see class Response for details
     */
    @Override
    public Response serve(IHTTPSession session) {
        Map<String, String> files = new HashMap<String, String>();
        Method method = session.getMethod();
        if (Method.PUT.equals(method) || Method.POST.equals(method)) {
            try {
                session.parseBody(files);
            } catch (IOException ioe) {
                return newFixedLengthResponse(Response.Status.INTERNAL_ERROR, NanoHTTPD.MIME_PLAINTEXT, "SERVER INTERNAL ERROR: IOException: " + ioe.getMessage());
            } catch (ResponseException re) {
                return newFixedLengthResponse(re.getStatus(), NanoHTTPD.MIME_PLAINTEXT, re.getMessage());
            }
        }

        Map<String, String> parms = this.getParms(session.getParameters());
        return serve(session.getUri(), method, session.getHeaders(), parms, files);
    }

    /**
     * Override this to customize the server.
     * <p/>
     * <p/>
     * (By default, this returns a 404 "Not Found" plain text error response.)
     *
     * @param uri
     *            Percent-decoded URI without parameters, for example
     *            "/index.cgi"
     * @param method
     *            "GET", "POST" etc.
     * @param parameters
     *            Parsed, percent decoded parameters from URI and, in case of
     *            POST, data.
     * @param headers
     *            Header entries, percent decoded
     * @return HTTP response, see class Response for details
     */
    @Override
    public Response serve(String uri, Method method, Map<String, String> headers, Map<String, String> parameters, Map<String, String> files) {
        Log.d(LOGTAG, "uri >> " + uri);
        Log.d(LOGTAG, "parameters >> " + parameters);

        String mimeType;
        Response response;
        Response.Status status;

        FileContentResolver fileContent = new FileContentResolver(this.assetManager);

        StringBuffer html = new StringBuffer();


        if ("/".equals(uri) || "/index.html".equals(uri)) {
            mimeType = NanoHTTPD.MIME_HTML;
            status = Response.Status.OK;

            html.append(fileContent.getTextFileContent("/index.html"));

            response = newFixedLengthResponse(status, mimeType, html.toString());
        }
        else if ("/request.html".equals(uri)) {
            mimeType = NanoHTTPD.MIME_HTML;
            status = Response.Status.OK;

            html.append(fileContent.getTextFileContent("/request.html"));

            response = newFixedLengthResponse(status, mimeType, html.toString());
        }
        else if ("/api/shop/info".equals(uri)) {
            mimeType = "application/json";
            status = Response.Status.OK;

            JSONArray shopInfo = new ShopApiServlet(this.context).service();

            response = newFixedLengthResponse(status, mimeType, shopInfo.toString());
        }
        else if ("/api/m/list".equals(uri)) {
            mimeType = "application/json";
            status = Response.Status.OK;

            String ret = null;
            JSONArray musicList = null;
            try {
                musicList = new MusicApiServlet(this.context).serviceMusicList(parameters);
                ret = musicList.toString();
            }
            catch(Exception e) {
                ret = e.getMessage();
                e.printStackTrace();
                Log.e("WebServer", e.getMessage());
            }

            response = newFixedLengthResponse(status, mimeType, ret);
        }
        else if ("/api/m/q/find".equals(uri)) {
            mimeType = "application/json";
            status = Response.Status.OK;

            boolean hasMusic = new MusicApiServlet(this.context).alreadyGotMusicInQueueList(parameters);
            JSONObject json = new JSONObject();
            try {
                json.put("result", hasMusic);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            response = newFixedLengthResponse(status, mimeType, json.toString());
        }
        else if ("/api/m/q/list".equals(uri)) {
            mimeType = "application/json";
            status = Response.Status.OK;

            String ret = null;
            JSONArray queueing = null;

            try {
                queueing = new MusicApiServlet(this.context).serviceQueueing();
                ret = queueing.toString();
            }
            catch(Exception e) {
                ret = e.getMessage();
                e.printStackTrace();
                Log.e("WebServer", e.getMessage());
            }

            response = newFixedLengthResponse(status, mimeType, ret);
        }
        else if ("/api/m/q/add".equals(uri)) {
            mimeType = "application/json";
            status = Response.Status.OK;

            String ret = null;
            JSONArray queueing = null;
            try {
                queueing = new MusicApiServlet(this.context).addIntoQueue(method, parameters);
                ret = queueing.toString();
            }
            catch (Exception e) {
                e.printStackTrace();
                Log.e(LOGTAG, e.getMessage());
                ret = e.getMessage();
            }

            response = newFixedLengthResponse(status, mimeType, ret);
        }
        else {
            MimeTypeFinder mimeTypeFinder = new MimeTypeFinder();

            mimeType = mimeTypeFinder.findByExtension(uri);
            Log.d(LOGTAG, "mime-type >> " + mimeType);
            status = Response.Status.OK;

            if (mimeType != null) {

                if ("image/png".equals(mimeType) || "image/jpeg".equals(mimeType) || "image/gif".equals(mimeType)) {

                    try {
                        status = Response.Status.OK;
                        InputStream image = fileContent.getImageFileContent(uri);
                        response = newFixedLengthResponse(status, mimeType, image, image.available());
                    }
                    catch (Exception e) {
                        Log.e(LOGTAG, e.getMessage());
                        e.printStackTrace();

                        status = Response.Status.EXPECTATION_FAILED;
                        response = newFixedLengthResponse(status, mimeType, "");
                    }

                }
                else {
                    html.append(fileContent.getTextFileContent(uri));
                    response = newFixedLengthResponse(status, mimeType, html.toString());
                }
            }
            else { // case of mimeType is null
                mimeType = NanoHTTPD.MIME_HTML;
                status = Response.Status.NOT_FOUND;

                html.append("<html><body>");
                html.append("<h1>Not Found: " + uri + "</h1>");
                html.append("</body></html>");

                response = newFixedLengthResponse(status, mimeType, html.toString());
            }
        }

        return response;
    }

    class FileContentResolver {
        private AssetManager assetManager;

        FileContentResolver(AssetManager assetManager) {
            this.assetManager = assetManager;
        }

        InputStream getImageFileContent(String uri) throws Exception {
            return this.assetManager.open("www" + uri);
        }

        String getTextFileContent(String uri) {
            InputStream is = null;
            Writer writer = new StringWriter();

            char[] buffer = new char[2048];
            try {
                is = this.assetManager.open("www" + uri);
                Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                int n;
                while ((n = reader.read(buffer)) != -1) {
                    writer.write(buffer, 0, n);
                }
            }
            catch(Exception e) {
                Log.e(LOGTAG, e.getMessage());
                e.printStackTrace();
            }
            finally {
                try {if (is != null) is.close();} catch(Exception e) {Log.e(LOGTAG, e.getMessage()); e.printStackTrace();}
            }

            return writer.toString();
        }
    }

    class MimeTypeFinder {
        String findByExtension(String uri) {
            if (uri != null) {
                uri = uri.toLowerCase();
                if (uri.endsWith(".html") || uri.endsWith(".htm")) {
                    return "text/html";
                }
                else if (uri.endsWith(".css")) {
                    return "text/css";
                }
                else if (uri.endsWith(".js")) {
                    return "application/javascript";
                }
                else if (uri.endsWith(".ico")) {
                    return "image/x-icon";
                }
                else if (uri.endsWith(".png")) {
                    return "image/png";
                }
                else if (uri.endsWith(".gif")) {
                    return "image/gif";
                }
                else if (uri.endsWith(".jpg") || uri.endsWith(".jpeg") || uri.endsWith(".jpe")) {
                    return "image/jpeg";
                }
                else {
                    return "text/plain";
                }
            }
            else {
                return null;
            }
        }
    }
}
