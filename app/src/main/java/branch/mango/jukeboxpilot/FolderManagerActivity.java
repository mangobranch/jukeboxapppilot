package branch.mango.jukeboxpilot;

import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FolderManagerActivity extends AppCompatActivity {
    private List<String> item = null;
    private List<String> path = null;
//    private String root = "/";
    private String root = "/storage";
    private TextView mPath;

    private ListView folderList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_folder_manager);

        folderList = (ListView) findViewById(R.id.folder_list);
        folderList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                System.out.println("position ==> " + position);
                File file = new File(path.get(position));
                if (file.isDirectory()) {
                    if (file.canRead())
                        getDir(path.get(position));
                    else {
                        new AlertDialog.Builder(FolderManagerActivity.this)
//                        .setIcon(R.drawable.ic_launcher)
                                .setTitle("[" + file.getName() + "] folder can't be read!")
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // TODO Auto-generated method stub
                                    }
                                }).show();
                    }
                }
                else {
                    new AlertDialog.Builder(FolderManagerActivity.this)
//                    .setIcon(R.drawable.ic_launcher)
                            .setTitle("[" + file.getName() + "]")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // TODO Auto-generated method stub
                                }
                            }).show();
                }
            }
        });

        mPath = (TextView) findViewById(R.id.path);
        getDir(root);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_folder_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_favorite:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getDir(String dirPath) {
        mPath.setText("Location: " + dirPath);
        item = new ArrayList<String>();
        path = new ArrayList<String>();
        File f = new File(dirPath);
        File[] files = f.listFiles();
        if (!dirPath.equals(root)) {
            item.add(root);
            path.add(root);
            item.add("../");
            path.add(f.getParent());
        }

        for (int i = 0; i < files.length; i++) {
            File file = files[i];
//            path.add(file.getPath());
//            if (file.isDirectory())
//                item.add(file.getName() + "/");
//            else
//                item.add(file.getName());

            if (file.isDirectory()) {
                path.add(file.getPath());
                item.add(file.getName() + "/");
            }

        }
        ArrayAdapter<String> fileList = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, item);
//        setListAdapter(fileList);

        folderList.setAdapter(fileList);
    }

    public void selectThisFolder(View view) {
        System.out.println("tvDir ==> " + mPath.getText());

        Intent intent = new Intent();
        Bundle bundle = new Bundle();

        bundle.putString("tvDir", mPath.getText().toString());
        intent.putExtras(bundle);
        setResult(RESULT_OK, intent);

        finish();
    }

    /*@Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        System.out.println("position ==> " + position);
        File file = new File(path.get(position));
        if (file.isDirectory()) {
            if (file.canRead())
                getDir(path.get(position));
            else {
                new AlertDialog.Builder(this)
//                        .setIcon(R.drawable.ic_launcher)
                        .setTitle("[" + file.getName() + "] folder can't be read!")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // TODO Auto-generated method stub
                            }
                        }).show();
            }
        }
        else {
            new AlertDialog.Builder(this)
//                    .setIcon(R.drawable.ic_launcher)
                    .setTitle("[" + file.getName() + "]")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                        }
                    }).show();
        }
    }*/
}
