package branch.mango.jukeboxpilot;

/**
 * Created by joe on 16. 11. 27.
 */

public final class Constants {
    public final static int SPLASH_TIME = 2000; // sec.

    public final static int SQLITE_VERSION = 3;
    public final static String SQLITE_DB = "jukedb.db";

    public final static int MESSAGE_SENDED = 1;

    public interface IntentRequest {
        int FOLDER_ACTIVITY = 0;
    }
}
