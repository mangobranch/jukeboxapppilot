package branch.mango.jukeboxpilot;

import android.content.res.Configuration;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import branch.mango.jukeboxpilot.db.DBManager;
import branch.mango.jukeboxpilot.server.IPUtil;
import branch.mango.jukeboxpilot.server.nanohttpd.WebServer;
import branch.mango.jukeboxpilot.util.FormValidator;
import fi.iki.elonen.NanoHTTPD;

public class WebserverActivity extends AppCompatActivity {
    final private String LOGTAG = "WebserverActivity";

    private String domain;
    final private int port = 8080;
    private WebServer server;
    private boolean isServerOn = false;

    private DBManager dbManager;
    private JSONArray shopList;
    private boolean hasShop = false;

    private EditText etShopName;
    private EditText etTableCnt;
    private EditText etWifi;
    private EditText etPasswd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webserver);

        this.etShopName = (EditText) findViewById(R.id.editText_shopname);
        this.etTableCnt = (EditText) findViewById(R.id.editText_tablecount);
        this.etWifi = (EditText) findViewById(R.id.editText_wifiname);
        this.etPasswd = (EditText) findViewById(R.id.editText_pwd);

        /* initilizing SQLite */
        this.dbManager = new DBManager(getApplicationContext(), Constants.SQLITE_DB, null, Constants.SQLITE_VERSION);

        this.shopList = this.dbManager.select("select * from shop;");
        this.hasShop = shopList.length() > 0;

        if (this.hasShop) {
            try {
                JSONObject shop = this.shopList.getJSONObject(0);
                this.etShopName.setText(shop.getString("name"));
                this.etTableCnt.setText(shop.getString("tblCnt"));
                this.etWifi.setText(shop.getString("wifiNm"));
                this.etPasswd.setText(shop.getString("wifiPwd"));

                // SHOP 테이블에 저장된 서버실행여부를 조회해온다.
                if("Y".equals(shop.getString("serverFlag"))) {
                    this.isServerOn = true;
                }

            } catch (JSONException e) {
                e.printStackTrace();
                Log.e(LOGTAG, e.getMessage());
            }
        }

        /* getting local ip */
        this.domain = IPUtil.getLocalIPAdress(this);

        /* initilizing webserver */
        this.server = new WebServer(this.port);
        this.server.setContext(getApplicationContext());
        this.server.setAssesManager(getResources().getAssets());

        Log.d(LOGTAG, "isServerOn = " + isServerOn);
        Log.d(LOGTAG, "isAlive = " + this.server.isAlive());
        Log.d(LOGTAG, "getListeningPort = " + this.server.getListeningPort());

        // 서버가 살아있다면 URL  정보와 서버 버튼을 셋팅해준다.
        if(this.isServerOn || this.server.isAlive() || this.server.getListeningPort() != -1) {
            ((TextView) findViewById(R.id.textUrl)).setText("http://" + this.domain + ":8080/r.html");
            ((Button) findViewById(R.id.btnOnOff)).setText("Stop Server");
        }
        /* for testing... */
//        AlertDialog.Builder alert = new AlertDialog.Builder(WebserverActivity.this);
//        alert.setPositiveButton("확인", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss(); //닫기
//            }
//        });
//        alert.setMessage(this.dbManager.printData());
//        alert.show();
    }

    @Override
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    public void clickServerOnOff(View view) {

        FormValidator validator = new FormValidator();

        String shopName = this.etShopName.getText().toString();
        String tableCount = this.etTableCnt.getText().toString();
        String wifi = this.etWifi.getText().toString();
        String passwd = this.etPasswd.getText().toString();
        String updateSql = "";

        if (tableCount == null || tableCount.trim().equals("")) tableCount = "0";

        Log.d(LOGTAG, "shop name => " + shopName);
        Log.d(LOGTAG, "tableCount => " + tableCount);
        Log.d(LOGTAG, "wifi => " + wifi);
        Log.d(LOGTAG, "passwd => " + passwd);
        Log.d(LOGTAG, "isServerOn => " + isServerOn);

        String message = null;
        if (!validator.validate(shopName, "str")) message = "상점명을 입력하세요";
        else if (!validator.validate(Integer.valueOf(tableCount), "int")) message = "테이블수를 입력하세요";
        else if (!validator.validate(wifi, "str")) message = "WI-FI를 입력하세요";
        else if (!validator.validate(passwd, "str")) message = "WI-FI 비밀번호를 입력하세요";

        try {
            JSONObject shop = this.shopList.getJSONObject(0);
            boolean updateFlag = false;
            if(!shop.getString("name").equals(shopName)) {
                updateFlag = true;
            }
            if(!shop.getString("tblCnt").equals(tableCount)) {
                updateFlag = true;
            }
            if(!shop.getString("wifiNm").equals(wifi)) {
                updateFlag = true;
            }
            if(!shop.getString("wifiPwd").equals(passwd)) {
                updateFlag = true;
            }

            if(updateFlag) {
                updateSql = "update shop set name = '" + shopName + "', tblCnt = " + tableCount + ", wifiNm = '" + wifi + "', wifiPwd = '" + passwd + "'";
                this.dbManager.update(updateSql);
            }

        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(LOGTAG, e.getMessage());
        }

        if (message == null) {
            if (!this.hasShop) {
                String insertSql = "insert into shop values(null, '" + shopName + "', " + tableCount + ", '" + wifi + "', '" + passwd + "', 'N')";
                this.dbManager.insert(insertSql);
            }

            try {
                if (!this.isServerOn) {
                    this.server.start(NanoHTTPD.SOCKET_READ_TIMEOUT, false);
                    ((TextView) findViewById(R.id.textUrl)).setText("http://" + this.domain + ":8080/r.html");
                    ((Button) findViewById(R.id.btnOnOff)).setText("Stop Server");

                    updateSql = "update shop set serverFlag = 'Y'";
                    this.dbManager.update(updateSql);

                    this.isServerOn = true;
                    Log.d(LOGTAG, "Server started......");
                } else {
                    if (this.server != null) {
/*
                        this.etShopName.setText("");
                        this.etTableCnt.setText("");
                        this.etWifi.setText("");
                        this.etPasswd.setText("");
*/
                        this.server.stop();
                        ((TextView) findViewById(R.id.textUrl)).setText("");
                        ((Button) findViewById(R.id.btnOnOff)).setText("Start Server");

                        updateSql = "update shop set serverFlag = 'N'";
                        this.dbManager.update(updateSql);

                        this.isServerOn = false;
                        Log.d(LOGTAG, "Server stopped......");

                        Log.d(LOGTAG, "isAlive = " + this.server.isAlive());
                        Log.d(LOGTAG, "getListeningPort = " + this.server.getListeningPort());

                        // 서버를 멈추었지만 포트나 서버가 살아있다면 다시 중단시킴
                        if(this.server.isAlive() || this.server.getListeningPort() == -1) {
                            this.server.stop();
                        }

                    }
                }
            } catch (IOException ioe) {
                Log.e(LOGTAG, "Couldn't start server:\n" + ioe);

                /* 서버가 실행되고 있어서 오류가 났지만 일단 셋팅만 변경함 */
                ((TextView) findViewById(R.id.textUrl)).setText("http://" + this.domain + ":8080/r.html");
                ((Button) findViewById(R.id.btnOnOff)).setText("Stop Server");

                updateSql = "update shop set serverFlag = 'Y'";
                this.dbManager.update(updateSql);

                this.isServerOn = true;
                /* 서버가 살아있어서 오류가 났지만 일단 셋팅만 변경함 */

//                System.exit(-1);
            }
        }
        else {
            Snackbar.make(view, message, Snackbar.LENGTH_LONG).setAction("Action", null).show();
        }
    }

}
