package branch.mango.jukeboxpilot;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import branch.mango.jukeboxpilot.music.Music;
import branch.mango.jukeboxpilot.music.MusicListAdapter;
import branch.mango.jukeboxpilot.music.MusicManager;

public class QueueListActivity extends AppCompatActivity {
    private ListView queueList = null;
    private MusicListAdapter adapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_queue_list);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.menu_win10);

        List<Music> data = MusicManager.getInstatance().getMusicQueueList();

        this.setData(data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                List<Music> data = MusicManager.getInstatance().getMusicQueueList();
                this.setData(data);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_music_list, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setQueryHint(getString(R.string.type_music_title));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String s) {

                List<Music> data = new ArrayList<Music>();
                List<Music> queueList = MusicManager.getInstatance().getMusicQueueList();

                if (s == null || "".equals(s)) {
                    data = queueList;
                }
                else {
                    for (Music music : queueList) {
                        if (music.getTitle().contains(s)) {
                            data.add(music);
                        }
                    }
                }

                setData(data);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });

        return true;
    }

    private void setData(List<Music> data) {
        this.adapter = new MusicListAdapter(QueueListActivity.this, R.layout.music_list_row, data);

        this.queueList = (ListView) findViewById(R.id.queue_list);
        this.queueList.setAdapter(this.adapter);
    }
}
