package branch.mango.jukeboxpilot.util;

/**
 * Created by joe on 16. 11. 27.
 */

public class FormValidator {

    public boolean validate(Object value, String type) {
        boolean checker = true;

        if ("STRING".equalsIgnoreCase(type) || "STR".equalsIgnoreCase(type)) {
            if (value instanceof String) {
                if (value == null || ((String)value).trim().equals("")) {
                    checker = false;
                }
            }
            else {
                checker= false;
            }
        }
        else if ("INTEGER".equalsIgnoreCase(type) || "INT".equalsIgnoreCase(type)) {
            if (value instanceof Integer) {
                if (value == null || ((Integer)value) <= 0) {
                    checker = false;
                }
            }
            else {
                checker= false;
            }
        }

        return checker;
    }

}
