package branch.mango.jukeboxpilot;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.sqlite.SQLiteDatabase;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.List;

import branch.mango.jukeboxpilot.db.DBManager;
import branch.mango.jukeboxpilot.music.Music;
import branch.mango.jukeboxpilot.music.MusicLoader;
import branch.mango.jukeboxpilot.music.MusicManager;

public class MainActivity extends AppCompatActivity {
    public static Context mContext;

    private Button btnPlay;  //songs play
    private Button btnPause;  //pause
    private Button btnStop;  //stop
    private SeekBar seekBar;

    private TextView tvSn; //song name
    private TextView eTxtMusicPath;     //SD card path
    private CheckBox chkEndless;    //Q에 음악이 없을때 Pool에서 랜덤으로 꺼내옴.
    private Button btnSelDir;

    private DBManager dbManager;

    private MediaPlayer mediaPlayer = new MediaPlayer();

    private android.os.Handler adjustSeekBarHandler = new android.os.Handler();

    final private String FOLDER_PATH = "FOLDER_PATH";
    final private String MUSIC_TOTAL_SIZE = "MUSIC_TOTOAL_SIZE";
    
    private LinearLayout progressbarLayer;

    private MediaPlayer.OnCompletionListener onCompletion= new MediaPlayer.OnCompletionListener(){
        @Override
        public void onCompletion(MediaPlayer mp) {
            Music music = MusicManager.getInstatance().outQueue();
            if (music != null) {
                playMusic(music);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mContext = this;

        this.dbManager = new DBManager(getApplicationContext(), Constants.SQLITE_DB, null, Constants.SQLITE_VERSION);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        btnPlay = (Button) findViewById(R.id.btn_play);
        btnPause = (Button) findViewById(R.id.btn_pause);
        btnStop = (Button) findViewById(R.id.btn_stop);
        btnSelDir = (Button) findViewById(R.id.btn_select_dir);
        btnSelDir.requestFocus();

        tvSn = (TextView) findViewById(R.id.txtv_songname);
//        tvDir = (TextView) findViewById(R.id.txtv_dirPath);
        eTxtMusicPath=(TextView)findViewById(R.id.eTxtMusicPath);
        chkEndless=(CheckBox)findViewById(R.id.chkEndless);

        mediaPlayer.setOnCompletionListener(onCompletion);

        seekBar = (SeekBar) findViewById(R.id.seekBar);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    if (fromUser) {
                        mediaPlayer.seekTo(progress * 1000);
                    }
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {}

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {}
            }
        );

        String sdPath;
        JSONObject json = this.dbManager.selectOne("select value from code where code = '" + this.FOLDER_PATH+ "';");
        try {
            sdPath = json.getString("value");
        } catch (JSONException e) {
            sdPath = MusicManager.getInstatance().getSdCardPath();
        }
        eTxtMusicPath.setText(sdPath);

        try {
            JSONObject fileSizeJson = this.dbManager.selectOne("select value from code where code = '" + this.MUSIC_TOTAL_SIZE+ "';");
            long fileSize = MusicManager.getInstatance().folderSize(new File(sdPath));

            if (fileSizeJson.has("value") && !Long.toString(fileSize).equals(fileSizeJson.getString("value"))) {
                Log.d("MainActivity", "saved file size ==> " + fileSizeJson.getString("value"));
                Log.d("MainActivity", "file size ==> " + fileSize);

                scanMusicList();
            }
            else {
                JSONArray musicList = this.dbManager.select("select * from music;");
                for (int i = 0; i < musicList.length(); i++) {
                    MusicManager.getInstatance().addMusicToPoolList(new Music((JSONObject) musicList.get(i)));
                }
                btnSelDir.setText(MusicManager.getInstatance().getMusicPoolList().size() + " Songs Loaded");
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            Log.e("MainActivity", "Exception occurred during select music list");
        }

        chkEndless.setChecked(true);
        MusicManager.getInstatance().setBlnEndless(true);

        /* ProgressBar Layer */
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        progressbarLayer = (LinearLayout) inflater.inflate(R.layout.progressbar_layout, null);
        LinearLayout.LayoutParams paramsLayer = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
        );
        addContentView(progressbarLayer, paramsLayer);
        progressbarLayer.setVisibility(View.INVISIBLE);

        chkEndless.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View arg0) {
                if (chkEndless.isChecked()) {
                    MusicManager.getInstatance().setBlnEndless(true);
                }
                else {
                    MusicManager.getInstatance().setBlnEndless(false);
                }
            }
        });

        btnPlay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if(mediaPlayer.isPlaying()) {
                    mediaPlayer.pause();
                    MusicManager.getInstatance().setPlayStatus(2, MainActivity.this); // pause
                }
                else {
                    if (MusicManager.getInstatance().getPlayStatus() == 2) {
                        mediaPlayer.start();
                        MusicManager.getInstatance().setPlayStatus(1, MainActivity.this); // play
                    }
                    else {
                        Music music = MusicManager.getInstatance().outQueue();
                        if (music != null) {
                            playMusic(music);
                            MusicManager.getInstatance().setPlayStatus(1, MainActivity.this); // play
                        }
                    }
                }
            }
        });

        btnPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Music music = MusicManager.getInstatance().outQueue();
                if (music != null) {
                    mediaPlayer.stop();
                    playMusic(music);
                    MusicManager.getInstatance().setPlayStatus(1, MainActivity.this); // play
                }
            }
        });

        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                mediaPlayer.stop();
                seekBar.setProgress(0);
                MusicManager.getInstatance().setPlayStatus(0, MainActivity.this); // stop
            }
        });
    }

    @Override
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_setting) {
            Intent intent = new Intent(this, WebserverActivity.class);
            startActivity(intent);
            return true;
        }
        else if (id == R.id.action_music_list) {
            Intent intent = new Intent(this, MusicListActivity.class);
            startActivity(intent);
            return true;
        }
        else if (id == R.id.action_queue_list) {
            Intent intent = new Intent(this, QueueListActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mediaPlayer.release();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.IntentRequest.FOLDER_ACTIVITY) {
            switch (resultCode) {
                case RESULT_OK:
                    String path = data.getStringExtra("tvDir");
                    if (path != null) {
                        path = path.replaceAll("Location:","").trim();

                        this.dbManager.delete("delete from code where code = '" + FOLDER_PATH + "';");
                        this.dbManager.insert("insert into code values ('" + FOLDER_PATH + "', '" + path + "');");

                        long fileSize = MusicManager.getInstatance().folderSize(new File(path));
                        this.dbManager.delete("delete from code where code = '" + MUSIC_TOTAL_SIZE + "';");
                        this.dbManager.insert("insert into code values ('" + MUSIC_TOTAL_SIZE + "', '" + fileSize + "');");
                    }

                    eTxtMusicPath.setText(path);

                    scanMusicList();
                    break;
                case RESULT_CANCELED:
                    break;

            }
        }
    }

    public void showFolderManager(View view) {
        Intent intent = new Intent(this, FolderManagerActivity.class);
        startActivityForResult(intent, Constants.IntentRequest.FOLDER_ACTIVITY);
    }

    public void onClickFloating(View view) {
        Intent intent = new Intent(this, QueueListActivity.class);
        startActivity(intent);
    }

    public void playMusic(Music music){
        try {
            tvSn.setText(music.getTitle());
            Log.d("MainActivity > ", "songpath: " + music.getPath());
            if(mediaPlayer !=null) {
                mediaPlayer.reset();
            }

            if (mediaPlayer.isPlaying()) {
                mediaPlayer.stop();
            }

            mediaPlayer.setDataSource(music.getPath());
            mediaPlayer.prepare();

            final int songDuration = mediaPlayer.getDuration() / 1000;
            seekBar.setProgress(0);
            seekBar.setMax(songDuration);

            /* set song duration */
            int minutes = songDuration / 60;
            int seconds = songDuration % 60;

            /*Adjust string*/
            String middle;
            if(seconds < 10){
                middle = ":0";
            }
            else{
                middle = ":";
            }

            /*Set text*/
            String durationText = minutes + middle + seconds;
            ((TextView) findViewById(R.id.songDurationTime))
                    .setText(durationText, TextView.BufferType.NORMAL);

            mediaPlayer.start();

            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (!mediaPlayer.isPlaying() && 0 == MusicManager.getInstatance().getPlayStatus()) {
                        seekBar.setProgress(0);
                        return;
                    }

                    int currentPosition = mediaPlayer.getCurrentPosition() / 1000;
                    int minutes = currentPosition / 60;
                    int seconds = currentPosition % 60;

                    String middle;
                    if (seconds < 10) {
                        middle = ":0";
                    } else {
                        middle = ":";
                    }

                    String textToSet = minutes + middle + seconds;
                    TextView songLapTime = (TextView) findViewById(R.id.songLapTime);
                    songLapTime.setText(textToSet, TextView.BufferType.NORMAL);

                    seekBar.setProgress(currentPosition);
                    adjustSeekBarHandler.postDelayed(this,1000);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void scanMusicList() {
        String path=String.valueOf(eTxtMusicPath.getText());
        MusicManager.getInstatance().setSdCardPath(path);
        MusicManager.getInstatance().getMusicPoolList().clear();

//                AsyncTask<Integer, String, Integer> progressDialog = new ProgressbarDialog(MainActivity.this);
//                progressDialog.execute(100);

        ProgressDialog dialog = ProgressDialog.show(MainActivity.this, "Scanning...", "Scanning Musics...", true);

//                ProgressbarCircle progressbarCircle = new ProgressbarCircle(progressbarLayer);
//                progressbarCircle.start();

        MusicLoader musicLoader = new MusicLoader(dialog, new ProgressbarMessageHandler(), path);
        musicLoader.start();
    }
    class ProgressbarMessageHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch(msg.what) {
                case Constants.MESSAGE_SENDED:

                    String sql = null;
                    SQLiteDatabase db = dbManager.getDatabase();

                    long fileSize = MusicManager.getInstatance().folderSize(new File(MusicManager.getInstatance().getSdCardPath()));
                    db.execSQL("delete from code where code = '"+ MUSIC_TOTAL_SIZE +"';");
                    db.execSQL("insert into code values ('"+MUSIC_TOTAL_SIZE+"','"+fileSize+"');");

                    db.execSQL("delete from music;");

                    List<Music> poolList = MusicManager.getInstatance().getMusicPoolList();
                    for (Music music : poolList) {
                        sql = "insert into music (id,path,name,size,artistName,albumName)";
                        sql+= "values ("+music.getId()+", " +
                                "'"+music.getPath().replaceAll("'","''")+"', " +
                                "'"+music.getTitle().replaceAll("'","''")+"', " +
                                ""+music.getSize()+", " +
                                "'"+music.getArtistName().replaceAll("'","''")+"', " +
                                "'"+music.getAlbumName().replaceAll("'","''")+"');";
                        db.execSQL(sql);
                    }

                    db.close();
                    btnSelDir.setText(MusicManager.getInstatance().getMusicPoolList().size() + " Songs Loaded");
                    break;
            }
        }

    }
}
