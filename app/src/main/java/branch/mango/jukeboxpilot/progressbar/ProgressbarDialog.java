package branch.mango.jukeboxpilot.progressbar;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

/**
 * Created by joe on 17. 1. 27.
 */

public class ProgressbarDialog extends AsyncTask<Integer, String, Integer> {
    private ProgressDialog mDialog;
    private Context mContext;

    public ProgressbarDialog(Context context) {
        this.mContext = context;
    }

    @Override
    protected void onPreExecute() {
        mDialog = new ProgressDialog(this.mContext);
        mDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mDialog.setMessage("Start");
        mDialog.show();

        super.onPreExecute();
    }

    @Override
    protected Integer doInBackground(Integer... params) {
        final int taskCnt = params[0];
        publishProgress("max", Integer.toString(taskCnt));

        for (int i = 0; i < taskCnt; ++i) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            publishProgress("progress", Integer.toString(i), Integer.toString(i) + "곡 스캔 중...");
        }

        return taskCnt;
    }

    @Override
    protected void onPostExecute(Integer result) {
        mDialog.dismiss();
        Toast.makeText(mContext, Integer.toString(result) + "곡 스캔 완료", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onProgressUpdate(String... progress) {
        if (progress[0].equals("progress")) {
            mDialog.setProgress(Integer.parseInt(progress[1]));
            mDialog.setMessage(progress[2]);
        } else if (progress[0].equals("max")) {
            mDialog.setMax(Integer.parseInt(progress[1]));
        }
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }
}
