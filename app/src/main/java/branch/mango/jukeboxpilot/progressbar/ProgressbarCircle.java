package branch.mango.jukeboxpilot.progressbar;

import android.os.Handler;
import android.view.View;
import android.widget.LinearLayout;

/**
 * Created by joe on 17. 2. 4.
 */

public class ProgressbarCircle extends Thread {
    public boolean runnig = true;
    private LinearLayout progressbarLayer;
    final Handler barHandler = new Handler();

    public ProgressbarCircle(LinearLayout progressbarLayer) {
        this.progressbarLayer = progressbarLayer;
    }
    @Override
    public void run() {

        while (this.runnig) {
            barHandler.post(new Runnable() {
                @Override
                public void run() {
                    progressbarLayer.setVisibility(View.VISIBLE);
                }
            });
        }

        progressbarLayer.setVisibility(View.INVISIBLE);
    }
}
