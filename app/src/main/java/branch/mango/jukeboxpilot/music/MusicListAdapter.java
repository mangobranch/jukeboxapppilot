package branch.mango.jukeboxpilot.music;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import branch.mango.jukeboxpilot.R;

/**
 * Created by joe on 17. 2. 14.
 */

public class MusicListAdapter extends BaseAdapter {

    private Context context = null;
    private int layout = 0;
    private List<Music> data = null;
    private LayoutInflater inflater = null;

    public MusicListAdapter(Context context, int layout, List<Music> data) {
        this.context = context;
        this.layout = layout;
        this.data = data;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return this.data.size();
    }

    @Override
    public Object getItem(int i) {
        return this.data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        if (view == null) {
            view = this.inflater.inflate(this.layout, viewGroup, false);
        }

        TextView musicName = (TextView) view.findViewById(R.id.music_name);
        TextView musicianName = (TextView) view.findViewById(R.id.musician_name);
        TextView albumName = (TextView) view.findViewById(R.id.album_name);

        Music music = this.data.get(i);
        musicName.setText(music.getTitle());
        musicianName.setText(music.getArtistName());
        albumName.setText(music.getAlbumName());

        return view;
    }
}
