package branch.mango.jukeboxpilot.music;

import android.app.ProgressDialog;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.File;

import branch.mango.jukeboxpilot.Constants;
import branch.mango.jukeboxpilot.db.DBManager;

/**
 * Created by joe on 17. 2. 4.
 */

public class MusicLoader extends Thread {
    private int uniqueSeq = 0;

    private String path;
    private Handler messageHandler;
    private ProgressDialog dialog;

    public MusicLoader(ProgressDialog dialog, Handler messageHandler, String path) {
        this.dialog = dialog;
        this.messageHandler = messageHandler;
        this.path = path;
    }

    private boolean loadMusicList() {
        File dir = new File(MusicManager.getInstatance().getSdCardPath());
        File[] files = dir.listFiles();

        if (files != null) {
            try {
                if (files != null) {

                    for (File file: files) {
                        if (file.isDirectory()) {
                            MusicManager.getInstatance().setSdCardPath(file.getPath());
                            loadMusicList();
                        }
                        else if (file.getName().endsWith(".mp3") || file.getName().endsWith(".MP3")) {
                            Music music = new Music(
                                    this.uniqueSeq++,
                                    file.getPath(),
                                    file.getName().substring(0, (file.getName().length() - 4))
                            );

                            MusicManager.getInstatance().addMusicToPoolList(music);
                        }
                    }

                    // 지금은 일단 Pool을 그대로 Q에 넣고 시작하게 했음.
                    // musicQueueList= new ArrayList<Music>(musicPoolList);
                    return true;
                } else {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }
        else {
            return false;
        }
    }

    @Override
    public void run() {

        this.loadMusicList();
        Log.d("MusicLoader", "폴더(" + this.path +") 하위에 "+MusicManager.getInstatance().getMusicPoolList().size()+ "곡이 있습니다.");

        Message msg = this.messageHandler.obtainMessage();
        msg.what = Constants.MESSAGE_SENDED;
        this.messageHandler.sendMessage(msg);

        this.dialog.dismiss();
    }
}
