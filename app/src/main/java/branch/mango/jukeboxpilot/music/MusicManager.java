package branch.mango.jukeboxpilot.music;


import android.app.Activity;
import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.mp3.MP3AudioHeader;
import org.jaudiotagger.audio.mp3.MP3File;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.Tag;
import org.w3c.dom.Text;

import java.io.File;
import java.util.ArrayList;

import branch.mango.jukeboxpilot.R;


public class MusicManager {
    private static MusicManager instatance;

    private String sdCardPath;
    private boolean blnEndless = false; // 더이상 플레이 Q에 내용이 없을때에 Pool에서 당겨오게 할지를 설정함.
    private ArrayList<Music> musicQueueList = new ArrayList<Music>(); //선택한 곡을 넣을 Q리스트임.
    private ArrayList<Music> musicPoolList = new ArrayList<Music>(); //디렉토리 전체 파일을 풀에 올려 놓음 검색 및 랜덤용

    private int playStatus = 0; // 0: stop, 1: playing, 2: pause

    private MusicManager(){
        if(this.sdCardPath ==null){
            this.sdCardPath = Environment.getExternalStorageDirectory().toString() ;
        }
    }
    public static MusicManager getInstatance() {
        if (MusicManager.instatance == null) {
            MusicManager.instatance = new MusicManager();
        }
        return MusicManager.instatance;
    }

    public void setPlayStatus(int playStatus, Activity activity) {
        this.playStatus = playStatus;

        Button btnPlay = (Button) activity.findViewById(R.id.btn_play);
        TextView songLapTime = (TextView) activity.findViewById(R.id.songLapTime);

        switch(this.playStatus) {
            case 0: // stop
                songLapTime.setText("0:00", TextView.BufferType.NORMAL);
                btnPlay.setBackgroundResource(android.R.drawable.ic_media_play);
                break;
            case 1: // play
                btnPlay.setBackgroundResource(android.R.drawable.ic_media_pause);
                break;
            case 2: // pause
                btnPlay.setBackgroundResource(android.R.drawable.ic_media_play);
                break;
        }
    }
    public int getPlayStatus(){
        return this.playStatus;
    }

    public ArrayList<Music> getMusicPoolList() {
        return this.musicPoolList;
    }

    public void addMusicToPoolList(Music music) {
        try {
            MP3File mp3File = (MP3File) AudioFileIO.read(new File(music.getPath()));

            MP3AudioHeader audioHeader = (MP3AudioHeader) mp3File.getAudioHeader();
            music.setSize(audioHeader.getTrackLength());

            String albumName = "";
            String artistName = "";
//            Tag tag = mp3File.getTagOrCreateAndSetDefault();
            Tag tag = mp3File.getTag();

            if (tag != null) {
                if ("ALBUM".equals(FieldKey.ALBUM.name())) albumName = tag.getFirst(FieldKey.ALBUM);
                if ("ARTIST".equals(FieldKey.ALBUM.name())) artistName = tag.getFirst(FieldKey.ARTIST);
            }
            else {
                Log.d("MusicManager", "Tag is null");
                Log.d("MusicManager", "hasID3v1Tag => " + mp3File.hasID3v1Tag());
                Log.d("MusicManager", "hasID3v2Tag => " + mp3File.hasID3v2Tag());
            }

            music.setAlbumName(albumName);
            music.setArtistName(artistName);

            this.musicPoolList.add(music);

            Log.d("MusicManager", mp3File.displayStructureAsPlainText());
            Log.d("MusicManager", "----------------------------------------");
            Log.d("MusicManager", "@ID => " + music.getId());
            Log.d("MusicManager", "@TITLE => " + music.getTitle());
//            Log.d("MusicManager", "@TITLE => " + tag.getFirst(FieldKey.TITLE));
            Log.d("MusicManager", "@ALBUM => " + tag.getFirst(FieldKey.ALBUM));
//            Log.d("MusicManager", "@ALBUM_ARTIST => " + tag.getFirst(FieldKey.ALBUM_ARTIST));
            Log.d("MusicManager", "@ARTIST => " + tag.getFirst(FieldKey.ARTIST));
//            Log.d("MusicManager", "@ArtworkFactory => " + ArtworkFactory.getNew());
//            Log.d("MusicManager", "@Image URL => " + ArtworkFactory.getNew().getImageUrl());
//            Log.d("MusicManager", "@Image Object => " + ArtworkFactory.getNew().getImage());
//            Log.d("MusicManager", "@Image Picture Type => " + ArtworkFactory.getNew().getPictureType());
//
////            http://stackoverflow.com/questions/15683032/android-convert-base64-encoded-string-into-image-view
//
//            Log.d("MusicManager", "@Image byte[] => " + ArtworkFactory.getNew().getBinaryData());
//            Log.d("MusicManager", "@Image base64 converted => " + Base64.encodeToString(ArtworkFactory.getNew().getBinaryData(), 0));
            Log.d("MusicManager", "=====================================================================");
        }
        catch(Exception e) {
            e.printStackTrace();
            Log.e("MusicManager", e.getMessage());
        }
    }

    public void setBlnEndless(boolean blnEndless) {
        this.blnEndless = blnEndless;
        Log.d("MusicManager", "blnEndless: " + blnEndless);
    }

    public ArrayList<Music> getMusicQueueList() {
        return this.musicQueueList;
    }

    public void setMusicQueueList(Music music) {
        this.musicQueueList.add(music);
    }

    public String getSdCardPath() {
        return this.sdCardPath;
    }

    public void setSdCardPath(String sdCardPath) {
        this.sdCardPath = sdCardPath;
    }

    public Music outQueue(){
        Music music = null;
        if(0 < musicQueueList.size()) {
            music = this.musicQueueList.get(0);
            this.musicQueueList.remove(0);
        }
        else if(this.blnEndless) {
            Log.d("MusicManager","musicPoolList.size: "+ this.musicPoolList.size());
            if(0 < this.musicPoolList.size()) {
                music = this.musicPoolList.get(1 + (int) (Math.random() * this.musicPoolList.size() - 1));
            }
        }
        return music;
    }

    public long folderSize(File directory) {
        long length = 0;
        if (!directory.canExecute()) {
            return 0;
        }
        for (File file : directory.listFiles()) {
            if (file.isFile()) {
                length += file.length();
            }
            else {
                length += folderSize(file);
            }
        }
        return length;
    }

}
