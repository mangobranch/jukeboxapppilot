package branch.mango.jukeboxpilot.music;

import org.json.JSONObject;

public class Music {

    private int id;
    private String path;     //음악파일절대패스
    private String title;    //음악제목(파일제목,.mp3를 빼고)
    private int size = 0;
    private String artistName;
    private String albumName;

    public String getPath() {
        return path;
    }

    public Music (JSONObject json) throws Exception {
        this.id = json.getInt("id");
        this.path = json.getString("path");
        this.title = json.getString("name");
        this.size = json.getInt("size");
        this.artistName = json.getString("artistName");
        this.albumName = json.getString("albumName");
    }
    public Music(int id, String ssp, String sst){
        this.id = id;
        this.path =ssp;
        this.title =sst;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public String getAlbumName() {
        return albumName;
    }

    public void setAlbumName(String albumName) {
        this.albumName = albumName;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
